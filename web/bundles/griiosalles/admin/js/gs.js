$(document).ready(function(){
  $("#hote_ville").change(function() {
    quartierByCommune(this.options[this.selectedIndex].value);
  });
  $("#annexe_ville").change(function() {
    quartierByCommune(this.options[this.selectedIndex].value);
  });
});

function quartierByCommune(id_commune){
  $.ajax({
    url: "/griiosalles/web/app_dev.php/admin/quartier/by/commune",
    data: {id_commune: id_commune},
    type: "GET",
    success: function(response) {
      //alert("Cette publication a été chargés");
      var data = JSON.parse(response);
      var len = data.length;
      var codeHTML = '<label for="quartier">Quartier</label><select id="quartier" class="form-control" name="quartier">';
      for (var i = 0; i < len; i++) {
        codeHTML += '<option value="' + data[i].id + '">' + data[i].libelle + '</option>';
      }
      codeHTML += '</select>';
      $("#quartier-select-container").html(codeHTML);
    },
    error: function() {
      alert("Connexion non disponible");
    }
  });
}
