var server = 'http://localhost/griiosalles/web/app_dev.php';
$(document).ready(function(){

  $("#trigger-close-mobile-modal").click(function(){
    $("#close-modal-mobile").trigger('click');
    searchAdvancedMobile();
  });

  $("input:checkbox[name=equipement]").each(function(){
    $(this).change(function(){
      /*var ville = $("[name='ville']").val();
      var evenement = $("[name='evenement']").val();
      var nbrePlace = $("[name='place']").val();
      var salle = $("[name='salle']").val();
      var typePlace = $("[name='typeplace']").val();
      var equipements = [];
      $("input:checkbox[name=equipement]:checked").each(function(){
        equipements.push($(this).val());
      });*/
      searchAdvanced();
    });
  });
  $("[name='ville']").change(function(){
    searchAdvanced();
  });

  $("[name='evenement']").change(function(){
    searchAdvanced();
  });

  $("[name='place']").change(function(){
    searchAdvanced();
  });

  $("[name='salle']").change(function(){
    searchAdvanced();
  });

  $("[name='typeplace']").change(function(){
    searchAdvanced();
  });
});

function searchAdvancedMobile(){
  var ville = $("[name='ville2']").val();
  var evenement = $("[name='evenement2']").val();
  var nbrePlace = $("[name='place2']").val();
  var salle = $("[name='salle2']").val();
  var typePlace = $("[name='typeplace2']").val();
  var prix = $("#prixsalle").val();
  var equipements = [];
  $("input:checkbox[name=equipement2]:checked").each(function(){
    equipements.push($(this).val());
  });
  $.ajax({
    url: server + "/searchadv",
    data: {ville: ville.trim(), place: nbrePlace, salle: salle, evenement: evenement, typePlace: typePlace, equipements: equipements, prix: prix},
    type: "GET",
    success: function (response) {
      $('#salle-container').html(response);
    },
    error: function () {
      console.log('echec accès au serveur');
    }
  });
}

function searchAdvanced(){
  var ville = $("[name='ville']").val();
  var evenement = $("[name='evenement']").val();
  var nbrePlace = $("[name='place']").val();
  var salle = $("[name='salle']").val();
  var typePlace = $("[name='typeplace']").val();
  var prix = $("#prixsalle").val();
  var equipements = [];
  $("input:checkbox[name=equipement]:checked").each(function(){
    equipements.push($(this).val());
  });
  $.ajax({
    url: server + "/searchadv",
    data: {ville: ville.trim(), place: nbrePlace, salle: salle, evenement: evenement, typePlace: typePlace, equipements: equipements, prix: prix},
    type: "GET",
    success: function (response) {
      $('#salle-container').html(response);
    },
    error: function () {
      console.log('echec accès au serveur');
    }
  });
}
