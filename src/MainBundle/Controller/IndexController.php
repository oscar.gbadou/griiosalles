<?php

namespace MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AdminBundle\Entity\VueSalle;
use AdminBundle\Entity\VerifierDisponibiliteMail;
use AdminBundle\Entity\VerifierDisponibilite;
use AdminBundle\Entity\Referencement;
use AdminBundle\Entity\Newsletter;
use Symfony\Component\HttpFoundation\Response;

class IndexController extends Controller
{

  public function indexAction()
  {
    $em = $this->getDoctrine()->getManager();
    $salles = $em->getRepository('AdminBundle:Salle')->findBy(array(), array('id'=>'DESC'), 6, 0);
    $typeSalles = $em->getRepository('AdminBundle:TypeSalle')->findAll();
    return $this->render('MainBundle:Index:index.html.twig', array(
      'salles'=>$salles,
      'typeSalles'=>$typeSalles
    ));
  }

  public function referencementFormAction()
  {
    $em = $this->getDoctrine()->getManager();
    return $this->render('MainBundle:Index:referencementForm.html.twig');
  }

  public function newsletterAction(Request $request){
    $em = $this->getDoctrine()->getManager();
    $email = $request->get('email');
    $response = new Response();
    $response->headers->add(array('Access-Control-Allow-Origin' => '*'));
    $oldEmail = $em->getRepository('AdminBundle:Newsletter')->findOneByEmail($email);
    $message = \Swift_Message::newInstance()->setContentType('text/html')
            ->setSubject('Griio badges')
            ->setFrom(array('badges@griio.com' => 'GRIIO'))
            ->setTo($email)
            ->setBody($this->renderView('MainBundle:Index:newsletter.html.twig', array(
                'email' => $email,
    )));
    $this->get('mailer')->send($message);
    if(!$oldEmail){
      $newEmail = new Newsletter();
      $newEmail->setEmail($email)
      ->setDate(new \DateTime());
      $em->persist($newEmail);
      $em->flush();
      $response->setContent("SUCCESS");

    }else{
      $response->setContent("FOUND");
    }
    return $response;
  }

  public function saveReferencementAction(Request $request)
  {
    $em = $this->getDoctrine()->getManager();
    $newReferencement = new Referencement();
    $newReferencement->setNom($request->get('nom'))
    ->setVille($request->get('ville'))
    ->setQuartier($request->get('quartier'))
    ->setNbrePlace($request->get('nbrePlace'))
    ->setTypePlace($request->get('typePlace'))
    ->setResponsable($request->get('responsable'))
    ->setTelephone($request->get('telephone'))
    ->setEmail($request->get('email'));
    $em->persist($newReferencement);
    $em->flush();

    $message = \Swift_Message::newInstance()->setContentType('text/html')
            ->setSubject('Référencement de salle')
            ->setFrom(array('no-reply@griio.com' => 'GRIIO'))
            ->setTo($request->get('email'))
            ->setBody($this->renderView('MainBundle:Index:referencer_salle.html.twig', array(
                'user' => $newUserM,
                'confirmationUrl' => $_SERVER['HTTP_HOST'] . $this->generateUrl('user_registration_confirmation') . '?token=' . $newUserM->getConfirmationToken()
    )));
    $this->get('swiftmailer.mailer.first_mailer')->send($message);

    $this->get('session')->getFlashBag()->add('success', ' Votre requête a été enrégistrée avec succès');
    return $this->redirect($this->generateUrl('main_homepage'));
  }

  public function verifierDisponibiliteAction(Request $request){
    $em = $this->getDoctrine()->getManager();
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
      $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
      $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
      $ip = $_SERVER['REMOTE_ADDR'];
    }
    $salle = $em->getRepository('AdminBundle:Salle')->find($request->get('id'));
    $newVerifierDispo = new VerifierDisponibilite();
    $newVerifierDispo->setSalle($salle)
    ->setIp($ip);
    $em->persist($newVerifierDispo);
    $em->flush();
    $response = new Response();
    $response->headers->add(array('Access-Control-Allow-Origin' => '*'));
    $response->setContent("SUCCESS");
    return $response;
  }

  public function verifierDisponibiliteMailAction(Request $request, $id){
    $em = $this->getDoctrine()->getManager();
    $salle = $em->getRepository('AdminBundle:Salle')->find($id);
    $newVerifierDispoMail = new VerifierDisponibiliteMail();
    $newVerifierDispoMail->setSalle($salle)
    ->setObjet($request->get('objet'))
    ->setEmail($request->get('email'))
    ->setVille($request->get('ville'))
    ->setQuartier($request->get('quartier'))
    ->setTelephone($request->get('telephone'));
    $em->persist($newVerifierDispoMail);
    $em->flush();
    $this->get('session')->getFlashBag()->add('success', ' Votre mail a été envoyé avec succès');
    return $this->redirect($this->generateUrl('main_detail_salle', array('id'=>$id)));
  }

  public function setVueSalle($salle_id){
    $em = $this->getDoctrine()->getManager();
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
      $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
      $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
      $ip = $_SERVER['REMOTE_ADDR'];
    }
    $salle = $em->getRepository('AdminBundle:Salle')->find($salle_id);
    $now = new \DateTime();
    $lastView = $em->getRepository('AdminBundle:VueSalle')
    ->findOneBy(array('salle'=>$salle,'ip'=>$ip), array('date'=>'DESC'));
    if(!$lastView || ($lastView && $lastView->getDate()->format('Y-m-d') != $now->format('Y-m-d'))){
      $newVueSalle = new VueSalle();
      $newVueSalle->setIp($ip)
      ->setSalle($salle);
      $em->persist($newVueSalle);

      $salle->setNbreVue($salle->getNbreVue()+1);
      $em->flush();
    }
    return true;
  }

  public function detailSalleAction($id)
  {
    $em = $this->getDoctrine()->getManager();
    $salle = $em->getRepository('AdminBundle:Salle')->find($id);
    $this->setVueSalle($id);
    $salleSuggeres = $em->getRepository('AdminBundle:Salle')
    ->createQueryBuilder('s')
    ->leftJoin('s.hote', 'h')
    ->leftJoin('h.ville', 'v')
    ->where('v.id = :ville')
    ->setParameter('ville', $salle->getHote()->getVille()->getId())
    ->setFirstResult(0)
    ->setMaxResults(6)
    ->orderBy('s.id', 'DESC')
    ->getQuery()
    ->getResult();
    return $this->render('MainBundle:Index:detailSalle.html.twig', array(
      'salle'=>$salle,
      'salleSuggeres'=>$salleSuggeres
    ));
  }

  public function sallesAction(){
    $em = $this->getDoctrine()->getManager();
    $salles = $em->getRepository('AdminBundle:Salle')->findBy(array(), array('id'=>'DESC'));
    $typeEvenements = $em->getRepository('AdminBundle:TypeEvenement')->findAll();
    $typeSalles = $em->getRepository('AdminBundle:TypeSalle')->findAll();
    $equipements = $em->getRepository('AdminBundle:Equipement')->findAll();
    return $this->render('MainBundle:Index:salles.html.twig', array(
      'salles'=>$salles,
      'typeEvenements'=>$typeEvenements,
      'typeSalles'=>$typeSalles,
      'equipements'=>$equipements
    ));
  }

  public function searchAdvancedAction(Request $request){
    $em = $this->getDoctrine()->getManager();
    $typeEvenements = $em->getRepository('AdminBundle:TypeEvenement')->findAll();
    $typeSalles = $em->getRepository('AdminBundle:TypeSalle')->findAll();
    $equipements = $em->getRepository('AdminBundle:Equipement')->findAll();
    $q_ville = $request->get('ville');
    $q_evenement = $request->get('evenement');
    $q_typeSalle = $request->get('salle');
    $q_place = $request->get('place');
    $q_typeplace = $request->get('typeplace');
    $q_equipements = $request->get('equipements');
    $q_prix = $request->get('prix');

    $query = $em->getRepository('AdminBundle:Salle')
    ->createQUeryBuilder('s')
    ->orderBy('s.id', 'DESC')
    ->where('s.id > 0');
    $ville = null;
    if($q_ville){
      $ville = $em->getRepository('MainBundle:Communes')
      ->createQueryBuilder('c')
      ->where('c.label LIKE :ville')
      ->setParameter('ville', '%'.$q_ville.'%')
      ->getQuery()
      ->getResult();
      if($ville){
        $query->leftJoin('s.hote', 'h')
        ->leftJoin('h.ville', 'hv')
        ->leftJoin('s.annexe', 'a')
        ->leftJoin('a.ville', 'av')
        ->andWhere('hv.label LIKE :ville OR av.label LIKE :ville')
        ->setParameter('ville', '%'.$q_ville.'%');
      }
    }
    if($q_evenement){
      $evenement = $em->getRepository('AdminBundle:TypeEvenement')->find(intval($q_evenement));
      $query->andWhere('s.typeEvenementTag LIKE :evenement')
      ->setParameter('evenement', '%'.$evenement->getLibelle().'%');
    }
    if($q_place){
      if($q_typeplace){
        if($q_typeplace == "assis"){
          $query->andWhere('s.placeAssis >= :place');
        }elseif($q_typeplace == "debout"){
          $query->andWhere('s.placeDebout >= :place');
        }
      }else{
        $query->andWhere('s.placeAssis >= :place');
      }
      $query->setParameter('place', intval($q_place));
    }

    if($q_typeSalle){
      $typeSalle = $em->getRepository('AdminBundle:TypeSalle')->find(intval($q_typeSalle));
      if($typeSalle){
        $query->andWhere('s.typeSalleTag LIKE :typeSalle');
        $query->setParameter('typeSalle', '%'.$typeSalle->getLibelle().'%');
      }
    }

    if($q_equipements){
      foreach($q_equipements as $e){
        $equipement = $em->getRepository('AdminBundle:Equipement')->find(intval($e));
        $query->andWhere('s.equipementTag LIKE :equipementTag'.$e);
        $query->setParameter('equipementTag'.$e, '%'.$equipement->getLibelle().'%');
      }
    }

    if($q_prix){
      $query->andWhere('s.prix <= :prix')
      ->setParameter('prix', $q_prix);
    }

    $salles = $query->getQuery()->getResult();
    return $this->container->get('templating')->renderResponse('MainBundle:Index:resultSearch.html.twig', array(
      'salles'=>$salles,
      'ville'=>($ville && count($ville)>0)?$ville[0]: null
    ));
  }

  public function searchAction(Request $request){
    $em = $this->getDoctrine()->getManager();
    $typeEvenements = $em->getRepository('AdminBundle:TypeEvenement')->findAll();
    $typeSalles = $em->getRepository('AdminBundle:TypeSalle')->findAll();
    $equipements = $em->getRepository('AdminBundle:Equipement')->findAll();
    $q_ville = $request->get('ville');
    $q_typeSalle = $request->get('type');
    $q_place = $request->get('place');
    $query = $em->getRepository('AdminBundle:Salle')
    ->createQUeryBuilder('s');
    $ville = null;
    if($q_ville){
      $ville = $em->getRepository('MainBundle:Communes')
      ->createQueryBuilder('c')
      ->where('c.label LIKE :ville')
      ->setParameter('ville', '%'.$q_ville.'%')
      ->getQuery()
      ->getResult();
      if($ville){
        $query->leftJoin('s.hote', 'h')
        ->leftJoin('h.ville', 'hv')
        ->leftJoin('s.annexe', 'a')
        ->leftJoin('a.ville', 'av')
        ->where('hv.label LIKE :ville OR av.label LIKE :ville')
        ->setParameter('ville', '%'.$q_ville.'%');
      }
    }
    if($q_place){
      if($q_ville){
        $query->andWhere('s.placeAssis >= :place');
      }else{
        $query->where('s.placeAssis >= :place');
      }
      $query->setParameter('place', intval($q_place));
    }

    if($q_typeSalle){
      $typeSalle = $em->getRepository('AdminBundle:TypeSalle')->find(intval($q_typeSalle));
      if($typeSalle){
        if($q_ville || $q_place){
          $query->andWhere('s.typeSalleTag LIKE :typeSalle');
        }else{
          $query->where('s.typeSalleTag LIKE :typeSalle');
        }
        $query->setParameter('typeSalle', '%'.$typeSalle->getLibelle().'%');
      }
    }

    $salles = $query->getQuery()->getResult();

    return $this->render('MainBundle:Index:salles.html.twig', array(
      'salles'=>$salles,
      'typeEvenements'=>$typeEvenements,
      'typeSalles'=>$typeSalles,
      'equipements'=>$equipements
    ));
  }
}
