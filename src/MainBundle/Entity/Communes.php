<?php

namespace MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Communes
 */
class Communes
{
    /**
     * @var string
     */
    private $label;

    /**
     * @var integer
     */
    private $nbArrondissementsS;

    /**
     * @var integer
     */
    private $nbArrondissementsT;

    /**
     * @var integer
     */
    private $nbArrondissementsR;

    /**
     * @var integer
     */
    private $nbQuartiersS;

    /**
     * @var integer
     */
    private $nbQuartiersT;

    /**
     * @var integer
     */
    private $nbQuartiersR;

    /**
     * @var integer
     */
    private $nbCentreS;

    /**
     * @var integer
     */
    private $nbCentreT;

    /**
     * @var integer
     */
    private $nbCentreR;

    /**
     * @var integer
     */
    private $nbPosteS;

    /**
     * @var integer
     */
    private $nbPosteT;

    /**
     * @var integer
     */
    private $nbPosteR;

    /**
     * @var string
     */
    private $classements;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \MainBundle\Entity\Departements
     */
    private $departements;


    /**
     * Set label
     *
     * @param string $label
     * @return Communes
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label
     *
     * @return string 
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set nbArrondissementsS
     *
     * @param integer $nbArrondissementsS
     * @return Communes
     */
    public function setNbArrondissementsS($nbArrondissementsS)
    {
        $this->nbArrondissementsS = $nbArrondissementsS;

        return $this;
    }

    /**
     * Get nbArrondissementsS
     *
     * @return integer 
     */
    public function getNbArrondissementsS()
    {
        return $this->nbArrondissementsS;
    }

    /**
     * Set nbArrondissementsT
     *
     * @param integer $nbArrondissementsT
     * @return Communes
     */
    public function setNbArrondissementsT($nbArrondissementsT)
    {
        $this->nbArrondissementsT = $nbArrondissementsT;

        return $this;
    }

    /**
     * Get nbArrondissementsT
     *
     * @return integer 
     */
    public function getNbArrondissementsT()
    {
        return $this->nbArrondissementsT;
    }

    /**
     * Set nbArrondissementsR
     *
     * @param integer $nbArrondissementsR
     * @return Communes
     */
    public function setNbArrondissementsR($nbArrondissementsR)
    {
        $this->nbArrondissementsR = $nbArrondissementsR;

        return $this;
    }

    /**
     * Get nbArrondissementsR
     *
     * @return integer 
     */
    public function getNbArrondissementsR()
    {
        return $this->nbArrondissementsR;
    }

    /**
     * Set nbQuartiersS
     *
     * @param integer $nbQuartiersS
     * @return Communes
     */
    public function setNbQuartiersS($nbQuartiersS)
    {
        $this->nbQuartiersS = $nbQuartiersS;

        return $this;
    }

    /**
     * Get nbQuartiersS
     *
     * @return integer 
     */
    public function getNbQuartiersS()
    {
        return $this->nbQuartiersS;
    }

    /**
     * Set nbQuartiersT
     *
     * @param integer $nbQuartiersT
     * @return Communes
     */
    public function setNbQuartiersT($nbQuartiersT)
    {
        $this->nbQuartiersT = $nbQuartiersT;

        return $this;
    }

    /**
     * Get nbQuartiersT
     *
     * @return integer 
     */
    public function getNbQuartiersT()
    {
        return $this->nbQuartiersT;
    }

    /**
     * Set nbQuartiersR
     *
     * @param integer $nbQuartiersR
     * @return Communes
     */
    public function setNbQuartiersR($nbQuartiersR)
    {
        $this->nbQuartiersR = $nbQuartiersR;

        return $this;
    }

    /**
     * Get nbQuartiersR
     *
     * @return integer 
     */
    public function getNbQuartiersR()
    {
        return $this->nbQuartiersR;
    }

    /**
     * Set nbCentreS
     *
     * @param integer $nbCentreS
     * @return Communes
     */
    public function setNbCentreS($nbCentreS)
    {
        $this->nbCentreS = $nbCentreS;

        return $this;
    }

    /**
     * Get nbCentreS
     *
     * @return integer 
     */
    public function getNbCentreS()
    {
        return $this->nbCentreS;
    }

    /**
     * Set nbCentreT
     *
     * @param integer $nbCentreT
     * @return Communes
     */
    public function setNbCentreT($nbCentreT)
    {
        $this->nbCentreT = $nbCentreT;

        return $this;
    }

    /**
     * Get nbCentreT
     *
     * @return integer 
     */
    public function getNbCentreT()
    {
        return $this->nbCentreT;
    }

    /**
     * Set nbCentreR
     *
     * @param integer $nbCentreR
     * @return Communes
     */
    public function setNbCentreR($nbCentreR)
    {
        $this->nbCentreR = $nbCentreR;

        return $this;
    }

    /**
     * Get nbCentreR
     *
     * @return integer 
     */
    public function getNbCentreR()
    {
        return $this->nbCentreR;
    }

    /**
     * Set nbPosteS
     *
     * @param integer $nbPosteS
     * @return Communes
     */
    public function setNbPosteS($nbPosteS)
    {
        $this->nbPosteS = $nbPosteS;

        return $this;
    }

    /**
     * Get nbPosteS
     *
     * @return integer 
     */
    public function getNbPosteS()
    {
        return $this->nbPosteS;
    }

    /**
     * Set nbPosteT
     *
     * @param integer $nbPosteT
     * @return Communes
     */
    public function setNbPosteT($nbPosteT)
    {
        $this->nbPosteT = $nbPosteT;

        return $this;
    }

    /**
     * Get nbPosteT
     *
     * @return integer 
     */
    public function getNbPosteT()
    {
        return $this->nbPosteT;
    }

    /**
     * Set nbPosteR
     *
     * @param integer $nbPosteR
     * @return Communes
     */
    public function setNbPosteR($nbPosteR)
    {
        $this->nbPosteR = $nbPosteR;

        return $this;
    }

    /**
     * Get nbPosteR
     *
     * @return integer 
     */
    public function getNbPosteR()
    {
        return $this->nbPosteR;
    }

    /**
     * Set classements
     *
     * @param string $classements
     * @return Communes
     */
    public function setClassements($classements)
    {
        $this->classements = $classements;

        return $this;
    }

    /**
     * Get classements
     *
     * @return string 
     */
    public function getClassements()
    {
        return $this->classements;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set departements
     *
     * @param \MainBundle\Entity\Departements $departements
     * @return Communes
     */
    public function setDepartements(\MainBundle\Entity\Departements $departements = null)
    {
        $this->departements = $departements;

        return $this;
    }

    /**
     * Get departements
     *
     * @return \MainBundle\Entity\Departements 
     */
    public function getDepartements()
    {
        return $this->departements;
    }
}
