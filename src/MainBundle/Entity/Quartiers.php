<?php

namespace MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Quartiers
 */
class Quartiers
{
    /**
     * @var string
     */
    private $label;

    /**
     * @var integer
     */
    private $nbCentreS;

    /**
     * @var integer
     */
    private $nbCentreT;

    /**
     * @var integer
     */
    private $nbCentreR;

    /**
     * @var integer
     */
    private $nbPosteS;

    /**
     * @var integer
     */
    private $nbPosteT;

    /**
     * @var integer
     */
    private $nbPosteR;

    /**
     * @var string
     */
    private $classements;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \MainBundle\Entity\Arrondissements
     */
    private $arrondissements;

    /**
     * @var \MainBundle\Entity\Communes
     */
    private $communes;

    /**
     * @var \MainBundle\Entity\Departements
     */
    private $departements;


    /**
     * Set label
     *
     * @param string $label
     * @return Quartiers
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label
     *
     * @return string 
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set nbCentreS
     *
     * @param integer $nbCentreS
     * @return Quartiers
     */
    public function setNbCentreS($nbCentreS)
    {
        $this->nbCentreS = $nbCentreS;

        return $this;
    }

    /**
     * Get nbCentreS
     *
     * @return integer 
     */
    public function getNbCentreS()
    {
        return $this->nbCentreS;
    }

    /**
     * Set nbCentreT
     *
     * @param integer $nbCentreT
     * @return Quartiers
     */
    public function setNbCentreT($nbCentreT)
    {
        $this->nbCentreT = $nbCentreT;

        return $this;
    }

    /**
     * Get nbCentreT
     *
     * @return integer 
     */
    public function getNbCentreT()
    {
        return $this->nbCentreT;
    }

    /**
     * Set nbCentreR
     *
     * @param integer $nbCentreR
     * @return Quartiers
     */
    public function setNbCentreR($nbCentreR)
    {
        $this->nbCentreR = $nbCentreR;

        return $this;
    }

    /**
     * Get nbCentreR
     *
     * @return integer 
     */
    public function getNbCentreR()
    {
        return $this->nbCentreR;
    }

    /**
     * Set nbPosteS
     *
     * @param integer $nbPosteS
     * @return Quartiers
     */
    public function setNbPosteS($nbPosteS)
    {
        $this->nbPosteS = $nbPosteS;

        return $this;
    }

    /**
     * Get nbPosteS
     *
     * @return integer 
     */
    public function getNbPosteS()
    {
        return $this->nbPosteS;
    }

    /**
     * Set nbPosteT
     *
     * @param integer $nbPosteT
     * @return Quartiers
     */
    public function setNbPosteT($nbPosteT)
    {
        $this->nbPosteT = $nbPosteT;

        return $this;
    }

    /**
     * Get nbPosteT
     *
     * @return integer 
     */
    public function getNbPosteT()
    {
        return $this->nbPosteT;
    }

    /**
     * Set nbPosteR
     *
     * @param integer $nbPosteR
     * @return Quartiers
     */
    public function setNbPosteR($nbPosteR)
    {
        $this->nbPosteR = $nbPosteR;

        return $this;
    }

    /**
     * Get nbPosteR
     *
     * @return integer 
     */
    public function getNbPosteR()
    {
        return $this->nbPosteR;
    }

    /**
     * Set classements
     *
     * @param string $classements
     * @return Quartiers
     */
    public function setClassements($classements)
    {
        $this->classements = $classements;

        return $this;
    }

    /**
     * Get classements
     *
     * @return string 
     */
    public function getClassements()
    {
        return $this->classements;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set arrondissements
     *
     * @param \MainBundle\Entity\Arrondissements $arrondissements
     * @return Quartiers
     */
    public function setArrondissements(\MainBundle\Entity\Arrondissements $arrondissements = null)
    {
        $this->arrondissements = $arrondissements;

        return $this;
    }

    /**
     * Get arrondissements
     *
     * @return \MainBundle\Entity\Arrondissements 
     */
    public function getArrondissements()
    {
        return $this->arrondissements;
    }

    /**
     * Set communes
     *
     * @param \MainBundle\Entity\Communes $communes
     * @return Quartiers
     */
    public function setCommunes(\MainBundle\Entity\Communes $communes = null)
    {
        $this->communes = $communes;

        return $this;
    }

    /**
     * Get communes
     *
     * @return \MainBundle\Entity\Communes 
     */
    public function getCommunes()
    {
        return $this->communes;
    }

    /**
     * Set departements
     *
     * @param \MainBundle\Entity\Departements $departements
     * @return Quartiers
     */
    public function setDepartements(\MainBundle\Entity\Departements $departements = null)
    {
        $this->departements = $departements;

        return $this;
    }

    /**
     * Get departements
     *
     * @return \MainBundle\Entity\Departements 
     */
    public function getDepartements()
    {
        return $this->departements;
    }
}
