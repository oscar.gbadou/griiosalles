<?php

namespace MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Departements
 */
class Departements
{
    /**
     * @var string
     */
    private $label;

    /**
     * @var integer
     */
    private $nbCommunesS;

    /**
     * @var integer
     */
    private $nbCommunesT;

    /**
     * @var integer
     */
    private $nbCommunesR;

    /**
     * @var integer
     */
    private $nbArrondissementsS;

    /**
     * @var integer
     */
    private $nbArrondissementsT;

    /**
     * @var integer
     */
    private $nbArrondissementsR;

    /**
     * @var integer
     */
    private $nbQuartiersS;

    /**
     * @var integer
     */
    private $nbQuartiersT;

    /**
     * @var integer
     */
    private $nbQuartiersR;

    /**
     * @var integer
     */
    private $nbCentreS;

    /**
     * @var integer
     */
    private $nbCentreT;

    /**
     * @var integer
     */
    private $nbCentreR;

    /**
     * @var integer
     */
    private $nbPosteS;

    /**
     * @var integer
     */
    private $nbPosteT;

    /**
     * @var integer
     */
    private $nbPosteR;

    /**
     * @var string
     */
    private $classements;

    /**
     * @var integer
     */
    private $id;


    /**
     * Set label
     *
     * @param string $label
     * @return Departements
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label
     *
     * @return string 
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set nbCommunesS
     *
     * @param integer $nbCommunesS
     * @return Departements
     */
    public function setNbCommunesS($nbCommunesS)
    {
        $this->nbCommunesS = $nbCommunesS;

        return $this;
    }

    /**
     * Get nbCommunesS
     *
     * @return integer 
     */
    public function getNbCommunesS()
    {
        return $this->nbCommunesS;
    }

    /**
     * Set nbCommunesT
     *
     * @param integer $nbCommunesT
     * @return Departements
     */
    public function setNbCommunesT($nbCommunesT)
    {
        $this->nbCommunesT = $nbCommunesT;

        return $this;
    }

    /**
     * Get nbCommunesT
     *
     * @return integer 
     */
    public function getNbCommunesT()
    {
        return $this->nbCommunesT;
    }

    /**
     * Set nbCommunesR
     *
     * @param integer $nbCommunesR
     * @return Departements
     */
    public function setNbCommunesR($nbCommunesR)
    {
        $this->nbCommunesR = $nbCommunesR;

        return $this;
    }

    /**
     * Get nbCommunesR
     *
     * @return integer 
     */
    public function getNbCommunesR()
    {
        return $this->nbCommunesR;
    }

    /**
     * Set nbArrondissementsS
     *
     * @param integer $nbArrondissementsS
     * @return Departements
     */
    public function setNbArrondissementsS($nbArrondissementsS)
    {
        $this->nbArrondissementsS = $nbArrondissementsS;

        return $this;
    }

    /**
     * Get nbArrondissementsS
     *
     * @return integer 
     */
    public function getNbArrondissementsS()
    {
        return $this->nbArrondissementsS;
    }

    /**
     * Set nbArrondissementsT
     *
     * @param integer $nbArrondissementsT
     * @return Departements
     */
    public function setNbArrondissementsT($nbArrondissementsT)
    {
        $this->nbArrondissementsT = $nbArrondissementsT;

        return $this;
    }

    /**
     * Get nbArrondissementsT
     *
     * @return integer 
     */
    public function getNbArrondissementsT()
    {
        return $this->nbArrondissementsT;
    }

    /**
     * Set nbArrondissementsR
     *
     * @param integer $nbArrondissementsR
     * @return Departements
     */
    public function setNbArrondissementsR($nbArrondissementsR)
    {
        $this->nbArrondissementsR = $nbArrondissementsR;

        return $this;
    }

    /**
     * Get nbArrondissementsR
     *
     * @return integer 
     */
    public function getNbArrondissementsR()
    {
        return $this->nbArrondissementsR;
    }

    /**
     * Set nbQuartiersS
     *
     * @param integer $nbQuartiersS
     * @return Departements
     */
    public function setNbQuartiersS($nbQuartiersS)
    {
        $this->nbQuartiersS = $nbQuartiersS;

        return $this;
    }

    /**
     * Get nbQuartiersS
     *
     * @return integer 
     */
    public function getNbQuartiersS()
    {
        return $this->nbQuartiersS;
    }

    /**
     * Set nbQuartiersT
     *
     * @param integer $nbQuartiersT
     * @return Departements
     */
    public function setNbQuartiersT($nbQuartiersT)
    {
        $this->nbQuartiersT = $nbQuartiersT;

        return $this;
    }

    /**
     * Get nbQuartiersT
     *
     * @return integer 
     */
    public function getNbQuartiersT()
    {
        return $this->nbQuartiersT;
    }

    /**
     * Set nbQuartiersR
     *
     * @param integer $nbQuartiersR
     * @return Departements
     */
    public function setNbQuartiersR($nbQuartiersR)
    {
        $this->nbQuartiersR = $nbQuartiersR;

        return $this;
    }

    /**
     * Get nbQuartiersR
     *
     * @return integer 
     */
    public function getNbQuartiersR()
    {
        return $this->nbQuartiersR;
    }

    /**
     * Set nbCentreS
     *
     * @param integer $nbCentreS
     * @return Departements
     */
    public function setNbCentreS($nbCentreS)
    {
        $this->nbCentreS = $nbCentreS;

        return $this;
    }

    /**
     * Get nbCentreS
     *
     * @return integer 
     */
    public function getNbCentreS()
    {
        return $this->nbCentreS;
    }

    /**
     * Set nbCentreT
     *
     * @param integer $nbCentreT
     * @return Departements
     */
    public function setNbCentreT($nbCentreT)
    {
        $this->nbCentreT = $nbCentreT;

        return $this;
    }

    /**
     * Get nbCentreT
     *
     * @return integer 
     */
    public function getNbCentreT()
    {
        return $this->nbCentreT;
    }

    /**
     * Set nbCentreR
     *
     * @param integer $nbCentreR
     * @return Departements
     */
    public function setNbCentreR($nbCentreR)
    {
        $this->nbCentreR = $nbCentreR;

        return $this;
    }

    /**
     * Get nbCentreR
     *
     * @return integer 
     */
    public function getNbCentreR()
    {
        return $this->nbCentreR;
    }

    /**
     * Set nbPosteS
     *
     * @param integer $nbPosteS
     * @return Departements
     */
    public function setNbPosteS($nbPosteS)
    {
        $this->nbPosteS = $nbPosteS;

        return $this;
    }

    /**
     * Get nbPosteS
     *
     * @return integer 
     */
    public function getNbPosteS()
    {
        return $this->nbPosteS;
    }

    /**
     * Set nbPosteT
     *
     * @param integer $nbPosteT
     * @return Departements
     */
    public function setNbPosteT($nbPosteT)
    {
        $this->nbPosteT = $nbPosteT;

        return $this;
    }

    /**
     * Get nbPosteT
     *
     * @return integer 
     */
    public function getNbPosteT()
    {
        return $this->nbPosteT;
    }

    /**
     * Set nbPosteR
     *
     * @param integer $nbPosteR
     * @return Departements
     */
    public function setNbPosteR($nbPosteR)
    {
        $this->nbPosteR = $nbPosteR;

        return $this;
    }

    /**
     * Get nbPosteR
     *
     * @return integer 
     */
    public function getNbPosteR()
    {
        return $this->nbPosteR;
    }

    /**
     * Set classements
     *
     * @param string $classements
     * @return Departements
     */
    public function setClassements($classements)
    {
        $this->classements = $classements;

        return $this;
    }

    /**
     * Get classements
     *
     * @return string 
     */
    public function getClassements()
    {
        return $this->classements;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}
