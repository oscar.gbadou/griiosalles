<?php

namespace MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Arrondissements
 */
class Arrondissements
{
    /**
     * @var integer
     */
    private $idlocalite;

    /**
     * @var string
     */
    private $label;

    /**
     * @var integer
     */
    private $nbQuartiersS;

    /**
     * @var integer
     */
    private $nbQuartiersT;

    /**
     * @var integer
     */
    private $nbQuartiersR;

    /**
     * @var integer
     */
    private $nbCentreS;

    /**
     * @var integer
     */
    private $nbCentreT;

    /**
     * @var integer
     */
    private $nbCentreR;

    /**
     * @var integer
     */
    private $nbPosteS;

    /**
     * @var integer
     */
    private $nbPosteT;

    /**
     * @var integer
     */
    private $nbPosteR;

    /**
     * @var string
     */
    private $classements;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \MainBundle\Entity\Communes
     */
    private $communes;

    /**
     * @var \MainBundle\Entity\Departements
     */
    private $departements;


    /**
     * Set idlocalite
     *
     * @param integer $idlocalite
     * @return Arrondissements
     */
    public function setIdlocalite($idlocalite)
    {
        $this->idlocalite = $idlocalite;

        return $this;
    }

    /**
     * Get idlocalite
     *
     * @return integer 
     */
    public function getIdlocalite()
    {
        return $this->idlocalite;
    }

    /**
     * Set label
     *
     * @param string $label
     * @return Arrondissements
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label
     *
     * @return string 
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set nbQuartiersS
     *
     * @param integer $nbQuartiersS
     * @return Arrondissements
     */
    public function setNbQuartiersS($nbQuartiersS)
    {
        $this->nbQuartiersS = $nbQuartiersS;

        return $this;
    }

    /**
     * Get nbQuartiersS
     *
     * @return integer 
     */
    public function getNbQuartiersS()
    {
        return $this->nbQuartiersS;
    }

    /**
     * Set nbQuartiersT
     *
     * @param integer $nbQuartiersT
     * @return Arrondissements
     */
    public function setNbQuartiersT($nbQuartiersT)
    {
        $this->nbQuartiersT = $nbQuartiersT;

        return $this;
    }

    /**
     * Get nbQuartiersT
     *
     * @return integer 
     */
    public function getNbQuartiersT()
    {
        return $this->nbQuartiersT;
    }

    /**
     * Set nbQuartiersR
     *
     * @param integer $nbQuartiersR
     * @return Arrondissements
     */
    public function setNbQuartiersR($nbQuartiersR)
    {
        $this->nbQuartiersR = $nbQuartiersR;

        return $this;
    }

    /**
     * Get nbQuartiersR
     *
     * @return integer 
     */
    public function getNbQuartiersR()
    {
        return $this->nbQuartiersR;
    }

    /**
     * Set nbCentreS
     *
     * @param integer $nbCentreS
     * @return Arrondissements
     */
    public function setNbCentreS($nbCentreS)
    {
        $this->nbCentreS = $nbCentreS;

        return $this;
    }

    /**
     * Get nbCentreS
     *
     * @return integer 
     */
    public function getNbCentreS()
    {
        return $this->nbCentreS;
    }

    /**
     * Set nbCentreT
     *
     * @param integer $nbCentreT
     * @return Arrondissements
     */
    public function setNbCentreT($nbCentreT)
    {
        $this->nbCentreT = $nbCentreT;

        return $this;
    }

    /**
     * Get nbCentreT
     *
     * @return integer 
     */
    public function getNbCentreT()
    {
        return $this->nbCentreT;
    }

    /**
     * Set nbCentreR
     *
     * @param integer $nbCentreR
     * @return Arrondissements
     */
    public function setNbCentreR($nbCentreR)
    {
        $this->nbCentreR = $nbCentreR;

        return $this;
    }

    /**
     * Get nbCentreR
     *
     * @return integer 
     */
    public function getNbCentreR()
    {
        return $this->nbCentreR;
    }

    /**
     * Set nbPosteS
     *
     * @param integer $nbPosteS
     * @return Arrondissements
     */
    public function setNbPosteS($nbPosteS)
    {
        $this->nbPosteS = $nbPosteS;

        return $this;
    }

    /**
     * Get nbPosteS
     *
     * @return integer 
     */
    public function getNbPosteS()
    {
        return $this->nbPosteS;
    }

    /**
     * Set nbPosteT
     *
     * @param integer $nbPosteT
     * @return Arrondissements
     */
    public function setNbPosteT($nbPosteT)
    {
        $this->nbPosteT = $nbPosteT;

        return $this;
    }

    /**
     * Get nbPosteT
     *
     * @return integer 
     */
    public function getNbPosteT()
    {
        return $this->nbPosteT;
    }

    /**
     * Set nbPosteR
     *
     * @param integer $nbPosteR
     * @return Arrondissements
     */
    public function setNbPosteR($nbPosteR)
    {
        $this->nbPosteR = $nbPosteR;

        return $this;
    }

    /**
     * Get nbPosteR
     *
     * @return integer 
     */
    public function getNbPosteR()
    {
        return $this->nbPosteR;
    }

    /**
     * Set classements
     *
     * @param string $classements
     * @return Arrondissements
     */
    public function setClassements($classements)
    {
        $this->classements = $classements;

        return $this;
    }

    /**
     * Get classements
     *
     * @return string 
     */
    public function getClassements()
    {
        return $this->classements;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set communes
     *
     * @param \MainBundle\Entity\Communes $communes
     * @return Arrondissements
     */
    public function setCommunes(\MainBundle\Entity\Communes $communes = null)
    {
        $this->communes = $communes;

        return $this;
    }

    /**
     * Get communes
     *
     * @return \MainBundle\Entity\Communes 
     */
    public function getCommunes()
    {
        return $this->communes;
    }

    /**
     * Set departements
     *
     * @param \MainBundle\Entity\Departements $departements
     * @return Arrondissements
     */
    public function setDepartements(\MainBundle\Entity\Departements $departements = null)
    {
        $this->departements = $departements;

        return $this;
    }

    /**
     * Get departements
     *
     * @return \MainBundle\Entity\Departements 
     */
    public function getDepartements()
    {
        return $this->departements;
    }
}
