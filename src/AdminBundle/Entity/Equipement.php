<?php

namespace AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Equipement
 *
 * @ORM\Table(name="equipement")
 * @ORM\Entity(repositoryClass="AdminBundle\Repository\EquipementRepository")
 */
class Equipement
{
  /**
  * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\Photo", cascade={"persist", "remove"})
  */
  private $icon;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=255)
     */
    private $libelle;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     * @return Equipement
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set icon
     *
     * @param \AdminBundle\Entity\Photo $icon
     * @return Equipement
     */
    public function setIcon(\AdminBundle\Entity\Photo $icon = null)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Get icon
     *
     * @return \AdminBundle\Entity\Photo
     */
    public function getIcon()
    {
        return $this->icon;
    }
}
