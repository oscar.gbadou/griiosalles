<?php

namespace AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * VerifierDisponibilite
 *
 * @ORM\Table(name="verifier_disponibilite")
 * @ORM\Entity(repositoryClass="AdminBundle\Repository\VerifierDisponibiliteRepository")
 */
class VerifierDisponibilite
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
    * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\Salle")
    */
    private $salle;

      /**
       * @var string
       *
       * @ORM\Column(name="ip", type="string", length=255)
       */
      private $ip;

      /**
       * @var \DateTime
       *
       * @ORM\Column(name="date", type="datetime")
       */
      private $date;

      public function __construct(){
        $this->date = new \DateTime();
      }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ip
     *
     * @param string $ip
     * @return VerifierDisponibilite
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip
     *
     * @return string 
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return VerifierDisponibilite
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set salle
     *
     * @param \AdminBundle\Entity\Salle $salle
     * @return VerifierDisponibilite
     */
    public function setSalle(\AdminBundle\Entity\Salle $salle = null)
    {
        $this->salle = $salle;

        return $this;
    }

    /**
     * Get salle
     *
     * @return \AdminBundle\Entity\Salle 
     */
    public function getSalle()
    {
        return $this->salle;
    }
}
