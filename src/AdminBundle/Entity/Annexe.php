<?php

namespace AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Annexe
 *
 * @ORM\Table(name="annexe")
 * @ORM\Entity(repositoryClass="AdminBundle\Repository\AnnexeRepository")
 */
class Annexe
{
  /**
  * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\Hote", inversedBy="annexes")
  */
  private $hote;

  /**
  * @ORM\ManyToOne(targetEntity="MainBundle\Entity\Communes")
  */
  private $ville;

  /**
  * @ORM\ManyToOne(targetEntity="MainBundle\Entity\Quartiers")
  */
  private $quartier;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="telephone", type="string", length=255)
     */
    private $telephone;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse", type="string", length=255)
     */
    private $adresse;

    /**
    * @var string
    *
    * @ORM\Column(name="lat", type="string", length=255, nullable=true)
    */
    private $lat;

    /**
    * @var string
    *
    * @ORM\Column(name="lon", type="string", length=255, nullable=true)
    */
    private $lon;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Annexe
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     * @return Annexe
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     * @return Annexe
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set ville
     *
     * @param \MainBundle\Entity\Communes $ville
     * @return Annexe
     */
    public function setVille(\MainBundle\Entity\Communes $ville = null)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return \MainBundle\Entity\Communes
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set quartier
     *
     * @param \MainBundle\Entity\Quartiers $quartier
     * @return Annexe
     */
    public function setQuartier(\MainBundle\Entity\Quartiers $quartier = null)
    {
        $this->quartier = $quartier;

        return $this;
    }

    /**
     * Get quartier
     *
     * @return \MainBundle\Entity\Quartiers
     */
    public function getQuartier()
    {
        return $this->quartier;
    }

    /**
     * Set hote
     *
     * @param \AdminBundle\Entity\Hote $hote
     * @return Annexe
     */
    public function setHote(\AdminBundle\Entity\Hote $hote = null)
    {
        $this->hote = $hote;

        return $this;
    }

    /**
     * Get hote
     *
     * @return \AdminBundle\Entity\Hote
     */
    public function getHote()
    {
        return $this->hote;
    }

    /**
     * Set lat
     *
     * @param string $lat
     * @return Annexe
     */
    public function setLat($lat)
    {
        $this->lat = $lat;

        return $this;
    }

    /**
     * Get lat
     *
     * @return string 
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * Set lon
     *
     * @param string $lon
     * @return Annexe
     */
    public function setLon($lon)
    {
        $this->lon = $lon;

        return $this;
    }

    /**
     * Get lon
     *
     * @return string 
     */
    public function getLon()
    {
        return $this->lon;
    }
}
