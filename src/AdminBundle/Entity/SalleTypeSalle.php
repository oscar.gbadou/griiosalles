<?php

namespace AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SalleTypeSalle
 *
 * @ORM\Table(name="salle_type_salle")
 * @ORM\Entity(repositoryClass="AdminBundle\Repository\SalleTypeSalleRepository")
 */
class SalleTypeSalle
{
  /**
  * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\TypeSalle")
  */
  private $typeSalle;

  /**
  * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\Salle", inversedBy="typeSalles")
  */
  private $salle;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set typeSalle
     *
     * @param \AdminBundle\Entity\TypeSalle $typeSalle
     * @return SalleTypeSalle
     */
    public function setTypeSalle(\AdminBundle\Entity\TypeSalle $typeSalle = null)
    {
        $this->typeSalle = $typeSalle;

        return $this;
    }

    /**
     * Get typeSalle
     *
     * @return \AdminBundle\Entity\TypeSalle
     */
    public function getTypeSalle()
    {
        return $this->typeSalle;
    }

    /**
     * Set salle
     *
     * @param \AdminBundle\Entity\Salle $salle
     * @return SalleTypeSalle
     */
    public function setSalle(\AdminBundle\Entity\Salle $salle = null)
    {
        $this->salle = $salle;

        return $this;
    }

    /**
     * Get salle
     *
     * @return \AdminBundle\Entity\Salle
     */
    public function getSalle()
    {
        return $this->salle;
    }
}
