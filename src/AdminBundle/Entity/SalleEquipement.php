<?php

namespace AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
* SalleEquipement
*
* @ORM\Table(name="salle_equipement")
* @ORM\Entity(repositoryClass="AdminBundle\Repository\SalleEquipementRepository")
*/
class SalleEquipement
{
  /**
  * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\Equipement")
  */
  private $equipement;

  /**
  * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\Salle", inversedBy="equipements")
  */
  private $salle;

  /**
  * @var int
  *
  * @ORM\Column(name="id", type="integer")
  * @ORM\Id
  * @ORM\GeneratedValue(strategy="AUTO")
  */
  private $id;


  /**
  * Get id
  *
  * @return integer
  */
  public function getId()
  {
    return $this->id;
  }

    /**
     * Set equipement
     *
     * @param \AdminBundle\Entity\Equipement $equipement
     * @return SalleEquipement
     */
    public function setEquipement(\AdminBundle\Entity\Equipement $equipement = null)
    {
        $this->equipement = $equipement;

        return $this;
    }

    /**
     * Get equipement
     *
     * @return \AdminBundle\Entity\Equipement
     */
    public function getEquipement()
    {
        return $this->equipement;
    }

    /**
     * Set salle
     *
     * @param \AdminBundle\Entity\Salle $salle
     * @return SalleEquipement
     */
    public function setSalle(\AdminBundle\Entity\Salle $salle = null)
    {
        $this->salle = $salle;

        return $this;
    }

    /**
     * Get salle
     *
     * @return \AdminBundle\Entity\Salle
     */
    public function getSalle()
    {
        return $this->salle;
    }
}
