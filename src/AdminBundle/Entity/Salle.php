<?php

namespace AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
* Salle
*
* @ORM\Table(name="salle")
* @ORM\Entity(repositoryClass="AdminBundle\Repository\SalleRepository")
*/
class Salle
{
  /**
  * @ORM\OneToMany(targetEntity="AdminBundle\Entity\VueSalle", mappedBy="salle", cascade={"remove"})
  */
  private $vues;

  /**
  * @ORM\OneToMany(targetEntity="AdminBundle\Entity\Photo", mappedBy="salle", cascade={"persist", "remove"})
  */
  private $photos;

  /**
  * @ORM\OneToMany(targetEntity="AdminBundle\Entity\SalleTypeSalle", mappedBy="salle", cascade={"persist", "remove"})
  */
  private $typeSalles;

  /**
  * @ORM\OneToMany(targetEntity="AdminBundle\Entity\SalleTypeEvenement", mappedBy="salle", cascade={"persist", "remove"})
  */
  private $typeEvenements;

  /**
  * @ORM\OneToMany(targetEntity="AdminBundle\Entity\SalleEquipement", mappedBy="salle", cascade={"persist", "remove"})
  */
  private $equipements;

  /**
  * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\Hote")
  */
  private $hote;
  /**
  * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\Annexe")
  */
  private $annexe;

  /**
  * @var int
  *
  * @ORM\Column(name="id", type="integer")
  * @ORM\Id
  * @ORM\GeneratedValue(strategy="AUTO")
  */
  private $id;

  /**
  * @var string
  *
  * @ORM\Column(name="apropos", type="text")
  */
  private $apropos;

  /**
  * @var int
  *
  * @ORM\Column(name="placeAssis", type="integer")
  */
  private $placeAssis;

  /**
  * @var int
  *
  * @ORM\Column(name="placeDebout", type="integer")
  */
  private $placeDebout;

  /**
  * @var string
  *
  * @ORM\Column(name="superficie", type="string", length=255)
  */
  private $superficie;

  /**
  * @var string
  *
  * @ORM\Column(name="nom", type="string", length=255)
  */
  private $nom;

  /**
  * @var string
  *
  * @ORM\Column(name="video", type="string", length=255, nullable=true)
  */
  private $video;

  /**
  * @var string
  *
  * @ORM\Column(name="typeSalleTag", type="string", length=255, nullable=true)
  */
  private $typeSalleTag;

  /**
  * @var string
  *
  * @ORM\Column(name="typeEvenementTag", type="string", length=255, nullable=true)
  */
  private $typeEvenementTag;

  /**
  * @var string
  *
  * @ORM\Column(name="equipementTag", type="string", length=255, nullable=true)
  */
  private $equipementTag;

  /**
   * @var int
   *
   * @ORM\Column(name="prix", type="integer")
   */
  private $prix;

  /**
   * @var int
   *
   * @ORM\Column(name="imgUne", type="integer", nullable=true)
   */
  private $imgUne;

  /**
  * @var integer
  *
  * @ORM\Column(name="nbreVue", type="integer")
  */
  private $nbreVue;

  /**
  * Get id
  *
  * @return integer
  */
  public function getId()
  {
    return $this->id;
  }

  /**
  * Set apropos
  *
  * @param string $apropos
  * @return Salle
  */
  public function setApropos($apropos)
  {
    $this->apropos = $apropos;

    return $this;
  }

  /**
  * Get apropos
  *
  * @return string
  */
  public function getApropos()
  {
    return $this->apropos;
  }

  /**
  * Set placeAssis
  *
  * @param integer $placeAssis
  * @return Salle
  */
  public function setPlaceAssis($placeAssis)
  {
    $this->placeAssis = $placeAssis;

    return $this;
  }

  /**
  * Get placeAssis
  *
  * @return integer
  */
  public function getPlaceAssis()
  {
    return $this->placeAssis;
  }

  /**
  * Set placeDebout
  *
  * @param integer $placeDebout
  * @return Salle
  */
  public function setPlaceDebout($placeDebout)
  {
    $this->placeDebout = $placeDebout;

    return $this;
  }

  /**
  * Get placeDebout
  *
  * @return integer
  */
  public function getPlaceDebout()
  {
    return $this->placeDebout;
  }

  /**
  * Set superficie
  *
  * @param string $superficie
  * @return Salle
  */
  public function setSuperficie($superficie)
  {
    $this->superficie = $superficie;

    return $this;
  }

  /**
  * Get superficie
  *
  * @return string
  */
  public function getSuperficie()
  {
    return $this->superficie;
  }

  /**
  * Set nom
  *
  * @param string $nom
  * @return Salle
  */
  public function setNom($nom)
  {
    $this->nom = $nom;

    return $this;
  }

  /**
  * Get nom
  *
  * @return string
  */
  public function getNom()
  {
    return $this->nom;
  }

  /**
  * Set hote
  *
  * @param \AdminBundle\Entity\Hote $hote
  * @return Salle
  */
  public function setHote(\AdminBundle\Entity\Hote $hote = null)
  {
    $this->hote = $hote;

    return $this;
  }

  /**
  * Get hote
  *
  * @return \AdminBundle\Entity\Hote
  */
  public function getHote()
  {
    return $this->hote;
  }

  /**
  * Set annexe
  *
  * @param \AdminBundle\Entity\Annexe $annexe
  * @return Salle
  */
  public function setAnnexe(\AdminBundle\Entity\Annexe $annexe = null)
  {
    $this->annexe = $annexe;

    return $this;
  }

  /**
  * Get annexe
  *
  * @return \AdminBundle\Entity\Annexe
  */
  public function getAnnexe()
  {
    return $this->annexe;
  }
  /**
  * Constructor
  */
  public function __construct()
  {
    $this->photos = new \Doctrine\Common\Collections\ArrayCollection();
  }

  /**
  * Add photos
  *
  * @param \AdminBundle\Entity\Photo $photos
  * @return Salle
  */
  public function addPhoto(\AdminBundle\Entity\Photo $photos)
  {
    $this->photos[] = $photos;

    return $this;
  }

  /**
  * Remove photos
  *
  * @param \AdminBundle\Entity\Photo $photos
  */
  public function removePhoto(\AdminBundle\Entity\Photo $photos)
  {
    $this->photos->removeElement($photos);
  }

  /**
  * Get photos
  *
  * @return \Doctrine\Common\Collections\Collection
  */
  public function getPhotos()
  {
    return $this->photos;
  }

  /**
  * Set video
  *
  * @param string $video
  * @return Salle
  */
  public function setVideo($video)
  {
    $this->video = $video;

    return $this;
  }

  /**
  * Get video
  *
  * @return string
  */
  public function getVideo()
  {
    return $this->video;
  }

  /**
  * Add typeSalles
  *
  * @param \AdminBundle\Entity\SalleTypeSalle $typeSalles
  * @return Salle
  */
  public function addTypeSalle(\AdminBundle\Entity\SalleTypeSalle $typeSalles)
  {
    $this->typeSalles[] = $typeSalles;

    return $this;
  }

  /**
  * Remove typeSalles
  *
  * @param \AdminBundle\Entity\SalleTypeSalle $typeSalles
  */
  public function removeTypeSalle(\AdminBundle\Entity\SalleTypeSalle $typeSalles)
  {
    $this->typeSalles->removeElement($typeSalles);
  }

  /**
  * Get typeSalles
  *
  * @return \Doctrine\Common\Collections\Collection
  */
  public function getTypeSalles()
  {
    return $this->typeSalles;
  }

  /**
  * Add typeEvenements
  *
  * @param \AdminBundle\Entity\SalleTypeEvenement $typeEvenements
  * @return Salle
  */
  public function addTypeEvenement(\AdminBundle\Entity\SalleTypeEvenement $typeEvenements)
  {
    $this->typeEvenements[] = $typeEvenements;

    return $this;
  }

  /**
  * Remove typeEvenements
  *
  * @param \AdminBundle\Entity\SalleTypeEvenement $typeEvenements
  */
  public function removeTypeEvenement(\AdminBundle\Entity\SalleTypeEvenement $typeEvenements)
  {
    $this->typeEvenements->removeElement($typeEvenements);
  }

  /**
  * Get typeEvenements
  *
  * @return \Doctrine\Common\Collections\Collection
  */
  public function getTypeEvenements()
  {
    return $this->typeEvenements;
  }

  /**
  * Add equipements
  *
  * @param \AdminBundle\Entity\SalleEquipement $equipements
  * @return Salle
  */
  public function addEquipement(\AdminBundle\Entity\SalleEquipement $equipements)
  {
    $this->equipements[] = $equipements;

    return $this;
  }

  /**
  * Remove equipements
  *
  * @param \AdminBundle\Entity\SalleEquipement $equipements
  */
  public function removeEquipement(\AdminBundle\Entity\SalleEquipement $equipements)
  {
    $this->equipements->removeElement($equipements);
  }

  /**
  * Get equipements
  *
  * @return \Doctrine\Common\Collections\Collection
  */
  public function getEquipements()
  {
    return $this->equipements;
  }

    /**
     * Set typeSalleTag
     *
     * @param string $typeSalleTag
     * @return Salle
     */
    public function setTypeSalleTag($typeSalleTag)
    {
        $this->typeSalleTag = $typeSalleTag;

        return $this;
    }

    /**
     * Get typeSalleTag
     *
     * @return string
     */
    public function getTypeSalleTag()
    {
        return $this->typeSalleTag;
    }

    /**
     * Set typeEvenementTag
     *
     * @param string $typeEvenementTag
     * @return Salle
     */
    public function setTypeEvenementTag($typeEvenementTag)
    {
        $this->typeEvenementTag = $typeEvenementTag;

        return $this;
    }

    /**
     * Get typeEvenementTag
     *
     * @return string
     */
    public function getTypeEvenementTag()
    {
        return $this->typeEvenementTag;
    }

    /**
     * Set equipementTag
     *
     * @param string $equipementTag
     * @return Salle
     */
    public function setEquipementTag($equipementTag)
    {
        $this->equipementTag = $equipementTag;

        return $this;
    }

    /**
     * Get equipementTag
     *
     * @return string
     */
    public function getEquipementTag()
    {
        return $this->equipementTag;
    }

    /**
     * Set prix
     *
     * @param integer $prix
     * @return Salle
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get prix
     *
     * @return integer
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * Set imgUne
     *
     * @param integer $imgUne
     * @return Salle
     */
    public function setImgUne($imgUne)
    {
        $this->imgUne = $imgUne;

        return $this;
    }

    /**
     * Get imgUne
     *
     * @return integer
     */
    public function getImgUne()
    {
        return $this->imgUne;
    }

    /**
     * Set nbreVue
     *
     * @param integer $nbreVue
     * @return Salle
     */
    public function setNbreVue($nbreVue)
    {
        $this->nbreVue = $nbreVue;

        return $this;
    }

    /**
     * Get nbreVue
     *
     * @return integer
     */
    public function getNbreVue()
    {
        return $this->nbreVue;
    }

    /**
     * Add vues
     *
     * @param \AdminBundle\Entity\VueSalle $vues
     * @return Salle
     */
    public function addVue(\AdminBundle\Entity\VueSalle $vues)
    {
        $this->vues[] = $vues;

        return $this;
    }

    /**
     * Remove vues
     *
     * @param \AdminBundle\Entity\VueSalle $vues
     */
    public function removeVue(\AdminBundle\Entity\VueSalle $vues)
    {
        $this->vues->removeElement($vues);
    }

    /**
     * Get vues
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVues()
    {
        return $this->vues;
    }
}
