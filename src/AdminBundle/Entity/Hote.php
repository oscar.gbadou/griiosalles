<?php

namespace AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
* Hote
*
* @ORM\Table(name="hote")
* @ORM\Entity(repositoryClass="AdminBundle\Repository\HoteRepository")
*/
class Hote
{

  /**
  * @ORM\OneToMany(targetEntity="AdminBundle\Entity\Annexe", mappedBy = "hote", cascade={"remove"})
  */
  private $annexes;

  /**
  * @ORM\ManyToOne(targetEntity="MainBundle\Entity\Communes")
  */
  private $ville;

  /**
  * @ORM\ManyToOne(targetEntity="MainBundle\Entity\Quartiers")
  */
  private $quartier;

  /**
  * @var int
  *
  * @ORM\Column(name="id", type="integer")
  * @ORM\Id
  * @ORM\GeneratedValue(strategy="AUTO")
  */
  private $id;

  /**
  * @var string
  *
  * @ORM\Column(name="nom", type="string", length=255)
  */
  private $nom;

  /**
  * @var string
  *
  * @ORM\Column(name="Adresse", type="string", length=255)
  */
  private $adresse;

  /**
  * @var string
  *
  * @ORM\Column(name="telephone", type="string", length=255)
  */
  private $telephone;

  /**
  * @var string
  *
  * @ORM\Column(name="siteweb", type="string", length=255)
  */
  private $siteweb;

  /**
  * @var string
  *
  * @ORM\Column(name="email", type="string", length=255)
  */
  private $email;

  /**
  * @var string
  *
  * @ORM\Column(name="facebook", type="string", length=255)
  */
  private $facebook;

  /**
  * @var string
  *
  * @ORM\Column(name="twitter", type="string", length=255)
  */
  private $twitter;

  /**
  * @var string
  *
  * @ORM\Column(name="instagram", type="string", length=255)
  */
  private $instagram;

  /**
  * @var string
  *
  * @ORM\Column(name="lat", type="string", length=255, nullable=true)
  */
  private $lat;

  /**
  * @var string
  *
  * @ORM\Column(name="lon", type="string", length=255, nullable=true)
  */
  private $lon;

  /**
  * @var \DateTime
  *
  * @ORM\Column(name="date", type="datetime")
  */
  private $date;

  public function __construct(){
    $this->date = new \DateTime();
  }


  /**
  * Get id
  *
  * @return integer
  */
  public function getId()
  {
    return $this->id;
  }

  /**
  * Set nom
  *
  * @param string $nom
  * @return Hote
  */
  public function setNom($nom)
  {
    $this->nom = $nom;

    return $this;
  }

  /**
  * Get nom
  *
  * @return string
  */
  public function getNom()
  {
    return $this->nom;
  }

  /**
  * Set adresse
  *
  * @param string $adresse
  * @return Hote
  */
  public function setAdresse($adresse)
  {
    $this->adresse = $adresse;

    return $this;
  }

  /**
  * Get adresse
  *
  * @return string
  */
  public function getAdresse()
  {
    return $this->adresse;
  }

  /**
  * Set telephone
  *
  * @param string $telephone
  * @return Hote
  */
  public function setTelephone($telephone)
  {
    $this->telephone = $telephone;

    return $this;
  }

  /**
  * Get telephone
  *
  * @return string
  */
  public function getTelephone()
  {
    return $this->telephone;
  }

  /**
  * Set siteweb
  *
  * @param string $siteweb
  * @return Hote
  */
  public function setSiteweb($siteweb)
  {
    $this->siteweb = $siteweb;

    return $this;
  }

  /**
  * Get siteweb
  *
  * @return string
  */
  public function getSiteweb()
  {
    return $this->siteweb;
  }

  /**
  * Set email
  *
  * @param string $email
  * @return Hote
  */
  public function setEmail($email)
  {
    $this->email = $email;

    return $this;
  }

  /**
  * Get email
  *
  * @return string
  */
  public function getEmail()
  {
    return $this->email;
  }

  /**
  * Set facebook
  *
  * @param string $facebook
  * @return Hote
  */
  public function setFacebook($facebook)
  {
    $this->facebook = $facebook;

    return $this;
  }

  /**
  * Get facebook
  *
  * @return string
  */
  public function getFacebook()
  {
    return $this->facebook;
  }

  /**
  * Set twitter
  *
  * @param string $twitter
  * @return Hote
  */
  public function setTwitter($twitter)
  {
    $this->twitter = $twitter;

    return $this;
  }

  /**
  * Get twitter
  *
  * @return string
  */
  public function getTwitter()
  {
    return $this->twitter;
  }

  /**
  * Set instagram
  *
  * @param string $instagram
  * @return Hote
  */
  public function setInstagram($instagram)
  {
    $this->instagram = $instagram;

    return $this;
  }

  /**
  * Get instagram
  *
  * @return string
  */
  public function getInstagram()
  {
    return $this->instagram;
  }

  /**
  * Set date
  *
  * @param \DateTime $date
  * @return Hote
  */
  public function setDate($date)
  {
    $this->date = $date;

    return $this;
  }

  /**
  * Get date
  *
  * @return \DateTime
  */
  public function getDate()
  {
    return $this->date;
  }

  /**
  * Set ville
  *
  * @param \MainBundle\Entity\Communes $ville
  * @return Hote
  */
  public function setVille(\MainBundle\Entity\Communes $ville = null)
  {
    $this->ville = $ville;

    return $this;
  }

  /**
  * Get ville
  *
  * @return \MainBundle\Entity\Communes
  */
  public function getVille()
  {
    return $this->ville;
  }

  /**
  * Set quartier
  *
  * @param \MainBundle\Entity\Quartiers $quartier
  * @return Hote
  */
  public function setQuartier(\MainBundle\Entity\Quartiers $quartier = null)
  {
    $this->quartier = $quartier;

    return $this;
  }

  /**
  * Get quartier
  *
  * @return \MainBundle\Entity\Quartiers
  */
  public function getQuartier()
  {
    return $this->quartier;
  }

    /**
     * Add annexes
     *
     * @param \AdminBundle\Entity\Annexe $annexes
     * @return Hote
     */
    public function addAnnex(\AdminBundle\Entity\Annexe $annexes)
    {
        $this->annexes[] = $annexes;

        return $this;
    }

    /**
     * Remove annexes
     *
     * @param \AdminBundle\Entity\Annexe $annexes
     */
    public function removeAnnex(\AdminBundle\Entity\Annexe $annexes)
    {
        $this->annexes->removeElement($annexes);
    }

    /**
     * Get annexes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAnnexes()
    {
        return $this->annexes;
    }

    /**
     * Set lat
     *
     * @param string $lat
     * @return Hote
     */
    public function setLat($lat)
    {
        $this->lat = $lat;

        return $this;
    }

    /**
     * Get lat
     *
     * @return string 
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * Set lon
     *
     * @param string $lon
     * @return Hote
     */
    public function setLon($lon)
    {
        $this->lon = $lon;

        return $this;
    }

    /**
     * Get lon
     *
     * @return string 
     */
    public function getLon()
    {
        return $this->lon;
    }
}
