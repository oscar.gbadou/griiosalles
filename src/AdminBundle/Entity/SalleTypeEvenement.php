<?php

namespace AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SalleTypeEvenement
 *
 * @ORM\Table(name="salle_type_evenement")
 * @ORM\Entity(repositoryClass="AdminBundle\Repository\SalleTypeEvenementRepository")
 */
class SalleTypeEvenement
{
  /**
  * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\TypeEvenement")
  */
  private $typeEvenement;

  /**
  * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\Salle", inversedBy="typeEvenements")
  */
  private $salle;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set typeEvenement
     *
     * @param \AdminBundle\Entity\TypeEvenement $typeEvenement
     * @return SalleTypeEvenement
     */
    public function setTypeEvenement(\AdminBundle\Entity\TypeEvenement $typeEvenement = null)
    {
        $this->typeEvenement = $typeEvenement;

        return $this;
    }

    /**
     * Get typeEvenement
     *
     * @return \AdminBundle\Entity\TypeEvenement
     */
    public function getTypeEvenement()
    {
        return $this->typeEvenement;
    }

    /**
     * Set salle
     *
     * @param \AdminBundle\Entity\Salle $salle
     * @return SalleTypeEvenement
     */
    public function setSalle(\AdminBundle\Entity\Salle $salle = null)
    {
        $this->salle = $salle;

        return $this;
    }

    /**
     * Get salle
     *
     * @return \AdminBundle\Entity\Salle
     */
    public function getSalle()
    {
        return $this->salle;
    }
}
