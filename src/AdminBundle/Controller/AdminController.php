<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use UserBundle\Entity\Admin;
use UserBundle\Form\AdminType;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\HttpFoundation\Request;

class AdminController extends Controller
{
  public function listAction()
  {
    $em = $this->getDoctrine()->getManager();
    $admin = $em->getRepository('UserBundle:Admin')->findAll();
    return $this->render('AdminBundle:Admin:list.html.twig', array(
      'admin'=>$admin
    ));
  }

  public function registerAction() {
    $discriminator = $this->container->get('pugx_user.manager.user_discriminator');
    $discriminator->setClass('UserBundle\Entity\Admin', true);
    $userManager = $this->container->get('pugx_user_manager');
    $newUser = new Admin();
    $form = $this->createForm(new AdminType(), $newUser);
    //        die(var_dump($form));
    $request = Request::createFromGlobals();
    $em = $this->getDoctrine()->getManager();
    $form->handleRequest($request);
    if ($request->getMethod() == 'POST') {
      //            $form->bind($request);
      if ($form->isValid()) {
        $newUserM = $userManager->createUser();

        $newUserM->setEmail($newUser->getEmail());
        $newUserM->setPlainPassword($newUser->getPlainPassword());
        $newUserM->setEnabled(true);
        $newUserM->addRole('ROLE_ADMIN');

        $userManager->updateUser($newUserM, true);

        $this->get('session')->getFlashBag()->add('success', 'Admin créé avec succès');
        return $this->redirect($this->generateUrl('admin_list_admin'));
      }
    }
    return $this->render('AdminBundle:Admin:register.html.twig', array(
      'form' => $form->createView(),
    ));
  }
}
