<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AdminBundle\Entity\Salle;
use AdminBundle\Entity\SalleTypeSalle;
use AdminBundle\Entity\SalleTypeEvenement;
use AdminBundle\Entity\SalleEquipement;
use AdminBundle\Form\SalleType;
use AdminBundle\Form\VideoType;
use AdminBundle\Form\PhotoType;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityRepository;

class SalleController extends Controller
{
  public function addAction($id, Request $request)
  {
    $em = $this->getDoctrine()->getManager();
    $hote = $em->getRepository('AdminBundle:Hote')->find($id);
    $typeSalles = $em->getRepository('AdminBundle:TypeSalle')->findAll();
    $typeEvenements = $em->getRepository('AdminBundle:TypeEvenement')->findAll();
    $equipements = $em->getRepository('AdminBundle:Equipement')->findAll();
    $newSalle = new Salle();
    $form = $this->createFormBuilder($newSalle)
    ->add('apropos', 'textarea', array(
      'label'=>'A propose de la salle',
      'attr'=>array(
        'class'=>'form-control',
        'style'=>'margin-bottom: 10px'
      )
    ))
    ->add('placeAssis', 'integer', array(
      'label'=>'Nombre de place assis',
      'attr'=>array(
        'class'=>'form-control',
        'style'=>'margin-bottom: 10px'
      )
    ))
    ->add('placeDebout', 'integer', array(
      'label'=>'Nombre de place debout',
      'attr'=>array(
        'class'=>'form-control',
        'style'=>'margin-bottom: 10px'
      )
    ))
    ->add('superficie', 'text', array(
      'label'=>'Superficie',
      'attr'=>array(
        'class'=>'form-control',
        'style'=>'margin-bottom: 10px'
      )
    ))
    ->add('nom', 'text', array(
      'label'=>'Nom',
      'attr'=>array(
        'class'=>'form-control',
        'style'=>'margin-bottom: 10px'
      )
    ))
    ->add('prix', 'integer', array(
      'label'=>'Prix',
      'attr'=>array(
        'class'=>'form-control',
        'style'=>'margin-bottom: 10px'
      )
    ))
    ->add('annexe', 'entity', array(
      'required'=>false,
      'label' => 'Annexe',
      'placeholder' => 'Choisir une annexe',
      'attr' => array(
        'class' => 'form-control',
        'style'=>'margin-bottom: 10px'
      ),
      'class' => 'AdminBundle:Annexe',
      'query_builder' => function (EntityRepository $er) use ($id) {
        return $er->createQueryBuilder('u')
        ->leftJoin('u.hote', 'h')
        ->where('h.id = :idhote')
        ->setParameter('idhote', $id);
      },
      'choice_label' => 'nom',
      ))
    /*->add('typeSalle', 'entity', array(
      'label' => 'Type de salle',
      'placeholder' => 'Choisir un type de salle',
      'attr' => array(
        'class' => 'form-control',
        'style'=>'margin-bottom: 10px'
      ),
      'class' => 'AdminBundle:TypeSalle',
      'choice_label' => 'libelle',
      ))
    ->add('typeEvenement', 'entity', array(
      'label' => 'Type de salle',
      'placeholder' => 'Choisir un type d\'événement',
      'attr' => array(
        'class' => 'form-control',
        'style'=>'margin-bottom: 10px'
      ),
      'class' => 'AdminBundle:TypeEvenement',
      'choice_label' => 'libelle',
      ))
    ->add('video', new VideoType())*/
    ->add('video', 'text', array(
      'label'=>'Vidéo',
      'required'=>false,
      'attr'=>array(
        'class'=>'form-control',
        'style'=>'margin-bottom: 10px',
        'placeholder'=>'https://youtu.be/RgKAFK5djSk'
      )
    ))
    ->add('photos', 'collection', array(
      'label'=>' ',
      'type' => new PhotoType(),
      'allow_add' => true,
      'allow_delete' => true,
      'by_reference' => true
      ))
    ->getForm();
    $form->handleRequest($request);
    /*$form = $this->createForm(new SalleType(), $newSalle);
    $form->handleRequest($request);*/
    if($request->getMethod() == 'POST'){
      if($form->isValid()){
        $newSalle = $form->getData();
        foreach($newSalle->getPhotos() as $p){
          $p->setSalle($newSalle);
          $em->persist($p);
        }
        $newSalle->setHote($hote);
        $em->persist($newSalle);
        $em->flush();

        $typeSalleTag = '';
        foreach($typeSalles as $s){
          if($request->get('type-salle'.$s->getId())){
            $newSalleTypeSalle = new SalleTypeSalle();
            $newSalleTypeSalle->setSalle($newSalle)
            ->setTypeSalle($s);
            $em->persist($newSalleTypeSalle);
            $em->flush();
            $typeSalleTag .= $s->getLibelle().' ';
          }
        }
        $newSalle->setTypeSalleTag($typeSalleTag);

        $typeEvenementTag = '';
        foreach($typeEvenements as $e){
          if($request->get('type-evenement'.$e->getId())){
            $newSalleTypeEvenement = new SalleTypeEvenement();
            $newSalleTypeEvenement->setSalle($newSalle)
            ->setTypeEvenement($e);
            $em->persist($newSalleTypeEvenement);
            $em->flush();
            $typeEvenementTag .= $e->getLibelle().' ';
          }
        }
        $newSalle->setTypeEvenementTag($typeEvenementTag);

        $equipementTag = '';
        foreach($equipements as $e){
          if($request->get('equipement'.$e->getId())){
            $newSalleEquipement = new SalleEquipement();
            $newSalleEquipement->setSalle($newSalle)
            ->setEquipement($e);
            $em->persist($newSalleEquipement);
            $em->flush();
            $equipementTag .= $e->getLibelle().' ';
          }
        }
        $newSalle->setEquipementTag($equipementTag);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success', ' Salle ajoutée avec succès');
        return $this->redirect($this->generateUrl('admin_salle_add', array('id'=>$id)));
      }
    }
    return $this->render('AdminBundle:Salle:add.html.twig', array(
      'form' => $form->createView(),
      'hote'=>$hote,
      'typeSalles'=>$typeSalles,
      'typeEvenements'=>$typeEvenements,
      'equipements'=>$equipements,
    ));
  }

  public function listAction()
  {
    $em = $this->getDoctrine()->getManager();
    $salles = $em->getRepository('AdminBundle:Salle')->findBy(array(), array('id'=>'DESC'));
    return $this->render('AdminBundle:Salle:list.html.twig', array(
      'salles'=>$salles
    ));
  }

  public function editAction($id, Request $request){
    $em = $this->getDoctrine()->getManager();
    $salle = $em->getRepository('AdminBundle:Salle')->find($id);
    $typeSalleId = array();
    foreach($salle->getTypeSalles() as $s){
      $typeSalleId[] = $s->getTypeSalle()->getId();
    }
    $typeEvenementId = array();
    foreach($salle->getTypeEvenements() as $s){
      $typeEvenementId[] = $s->getTypeEvenement()->getId();
    }
    $equipementId = array();
    foreach($salle->getEquipements() as $s){
      $equipementId[] = $s->getEquipement()->getId();
    }
    $typeSalles = $em->getRepository('AdminBundle:TypeSalle')->findAll();
    $typeEvenements = $em->getRepository('AdminBundle:TypeEvenement')->findAll();
    $equipements = $em->getRepository('AdminBundle:Equipement')->findAll();
    $form = $this->createFormBuilder($salle)
    ->add('apropos', 'textarea', array(
      'label'=>'A propose de la salle',
      'attr'=>array(
        'class'=>'form-control',
        'style'=>'margin-bottom: 10px'
      )
    ))
    ->add('placeAssis', 'integer', array(
      'label'=>'Nombre de place assis',
      'attr'=>array(
        'class'=>'form-control',
        'style'=>'margin-bottom: 10px'
      )
    ))
    ->add('placeDebout', 'integer', array(
      'label'=>'Nombre de place debout',
      'attr'=>array(
        'class'=>'form-control',
        'style'=>'margin-bottom: 10px'
      )
    ))
    ->add('superficie', 'text', array(
      'label'=>'Superficie',
      'attr'=>array(
        'class'=>'form-control',
        'style'=>'margin-bottom: 10px'
      )
    ))
    ->add('nom', 'text', array(
      'label'=>'Nom',
      'attr'=>array(
        'class'=>'form-control',
        'style'=>'margin-bottom: 10px'
      )
    ))
    ->add('prix', 'integer', array(
      'label'=>'Prix',
      'attr'=>array(
        'class'=>'form-control',
        'style'=>'margin-bottom: 10px'
      )
    ))
    ->add('annexe', 'entity', array(
      'required'=>false,
      'label' => 'Annexe',
      'placeholder' => 'Choisir une annexe',
      'attr' => array(
        'class' => 'form-control',
        'style'=>'margin-bottom: 10px'
      ),
      'class' => 'AdminBundle:Annexe',
      'query_builder' => function (EntityRepository $er) use ($id) {
        return $er->createQueryBuilder('u')
        ->leftJoin('u.hote', 'h')
        ->where('h.id = :idhote')
        ->setParameter('idhote', $id);
      },
      'choice_label' => 'nom',
      ))
    /*->add('typeSalle', 'entity', array(
      'label' => 'Type de salle',
      'placeholder' => 'Choisir un type de salle',
      'attr' => array(
        'class' => 'form-control',
        'style'=>'margin-bottom: 10px'
      ),
      'class' => 'AdminBundle:TypeSalle',
      'choice_label' => 'libelle',
      ))
    ->add('typeEvenement', 'entity', array(
      'label' => 'Type de salle',
      'placeholder' => 'Choisir un type d\'événement',
      'attr' => array(
        'class' => 'form-control',
        'style'=>'margin-bottom: 10px'
      ),
      'class' => 'AdminBundle:TypeEvenement',
      'choice_label' => 'libelle',
      ))
    ->add('video', new VideoType())*/
    ->add('video', 'text', array(
      'label'=>'Vidéo',
      'required'=>false,
      'attr'=>array(
        'class'=>'form-control',
        'style'=>'margin-bottom: 10px',
        'placeholder'=>'https://youtu.be/RgKAFK5djSk'
      )
    ))
    ->add('photos', 'collection', array(
      'label'=>' ',
      'type' => new PhotoType(),
      'allow_add' => true,
      'allow_delete' => true,
      'by_reference' => true
      ))
    ->getForm();
    $form->handleRequest($request);
    /*$form = $this->createForm(new SalleType(), $newSalle);
    $form->handleRequest($request);*/
    if($request->getMethod() == 'POST'){
      if($form->isValid()){
        $salle = $form->getData();
        /*foreach($newSalle->getPhotos() as $p){
          $p->setSalle($newSalle);
          $em->persist($p);
        }*/
        $em->persist($salle);
        $em->flush();
        foreach($salle->getTypeSalles() as $s){
          $em->remove($s);
          $em->flush();
        }
        foreach($salle->getTypeEvenements() as $s){
          $em->remove($s);
          $em->flush();
        }
        foreach($salle->getEquipements() as $s){
          $em->remove($s);
          $em->flush();
        }

        $typeSalleTags = '';
        foreach($typeSalles as $s){
          if($request->get('type-salle'.$s->getId())){
            $newSalleTypeSalle = new SalleTypeSalle();
            $newSalleTypeSalle->setSalle($salle)
            ->setTypeSalle($s);
            $em->persist($newSalleTypeSalle);
            $em->flush();
            $typeSalleTags .= $s->getLibelle().' ';
          }
        }
        $salle->setTypeSalleTag($typeSalleTags);

        $typeEvenementTags = '';
        foreach($typeEvenements as $e){
          if($request->get('type-evenement'.$e->getId())){
            $newSalleTypeEvenement = new SalleTypeEvenement();
            $newSalleTypeEvenement->setSalle($salle)
            ->setTypeEvenement($e);
            $em->persist($newSalleTypeEvenement);
            $em->flush();
            $typeEvenementTags .= $e->getLibelle().' ';
          }
        }
        $salle->setTypeEvenementTag($typeEvenementTags);

        $equipementTag = '';
        foreach($equipements as $e){
          if($request->get('equipement'.$e->getId())){
            $newSalleEquipement = new SalleEquipement();
            $newSalleEquipement->setSalle($salle)
            ->setEquipement($e);
            $em->persist($newSalleEquipement);
            $em->flush();
            $equipementTag .= $e->getLibelle().' ';
          }
        }
        $salle->setEquipementTag($equipementTag);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success', ' Salle ajoutée avec succès');
        return $this->redirect($this->generateUrl('admin_salle_list'));
      }
    }
    return $this->render('AdminBundle:Salle:edit.html.twig', array(
      'form' => $form->createView(),
      'hote'=>$salle->getHote(),
      'typeSalles'=>$typeSalles,
      'typeEvenements'=>$typeEvenements,
      'equipements'=>$equipements,
      'typeSalleId'=>$typeSalleId,
      'typeEvenementId'=>$typeEvenementId,
      'equipementId'=>$equipementId,
    ));
  }

  public function deleteAction($id){
    $em = $this->getDoctrine()->getManager();
    $salle = $em->getRepository('AdminBundle:Salle')->find($id);
    $em->remove($salle);
    $em->flush();
    return $this->redirect($this->generateUrl('admin_salle_list'));
  }

  public function photosAction($id){
    $em = $this->getDoctrine()->getManager();
    $salle = $em->getRepository('AdminBundle:Salle')->find($id);
    $photos = $em->getRepository('AdminBundle:Photo')->findBySalle($salle);
    return $this->render('AdminBundle:Salle:photos.html.twig', array(
      'photos'=>$photos,
      'salle'=>$salle
    ));
  }

  public function photoUneAction($id){
    $em = $this->getDoctrine()->getManager();
    $photo = $em->getRepository('AdminBundle:Photo')->find($id);
    $salle = $photo->getSalle();
    if($photo->getId() == $salle->getImgUne()){
      $salle->setImgUne(null);
    }else{
      $salle->setImgUne($photo->getId());
    }
    $em->flush();
    return $this->redirect($this->generateUrl('admin_salle_photos', array(
      'id'=>$salle->getId()
    )));
  }
}
