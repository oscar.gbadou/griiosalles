<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AdminBundle\Entity\Equipement;
use AdminBundle\Form\EquipementType;
use Symfony\Component\HttpFoundation\Request;

class EquipementController extends Controller
{
  public function addAction(Request $request)
  {
    $em = $this->getDoctrine()->getManager();
    $newEquipement = new Equipement();
    $form = $this->createForm(new EquipementType(), $newEquipement);
    $form->handleRequest($request);
    if($request->getMethod() == 'POST'){
      if($form->isValid()){
        $em->persist($newEquipement);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success', ' Equipement ajoutée avec succès');
        return $this->redirect($this->generateUrl('admin_type_salle_list'));
      }
    }
    return $this->render('AdminBundle:Equipement:add.html.twig', array(
      'form' => $form->createView(),
    ));
  }

  public function editAction($id, Request $request){
    $em = $this->getDoctrine()->getManager();
    $equipement = $em->getRepository('AdminBundle:Equipement')->find($id);
    $form = $this->createForm(new EquipementType(), $equipement);
    $form->handleRequest($request);
    if($request->getMethod() == 'POST'){
      if($form->isValid()){
        $em->persist($equipement);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success', ' Equipement mise à avec succès');
        return $this->redirect($this->generateUrl('admin_type_salle_list'));
      }
    }
    return $this->render('AdminBundle:Equipement:edit.html.twig', array(
      'form' => $form->createView(),
    ));
  }

  public function deleteAction($id){
    $em = $this->getDoctrine()->getManager();
    $equipement = $em->getRepository('AdminBundle:Equipement')->find($id);
    $em->remove($equipement);
    $em->flush();
    $this->get('session')->getFlashBag()->add('success', ' Equipement supprimé avec succès');
    return $this->redirect($this->generateUrl('admin_type_salle_list'));
  }
}
