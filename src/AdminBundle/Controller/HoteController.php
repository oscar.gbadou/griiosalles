<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AdminBundle\Entity\Hote;
use AdminBundle\Form\HoteType;
use AdminBundle\Form\HoteEditType;
use Symfony\Component\HttpFoundation\Request;

class HoteController extends Controller
{
  public function addAction(Request $request)
  {
    $em = $this->getDoctrine()->getManager();
    $newHote = new Hote();
    $form = $this->createForm(new HoteType(), $newHote);
    $form->handleRequest($request);
    if($request->getMethod() == 'POST'){
      if($form->isValid()){
        if($request->get('quartier')){
          $quartier = $em->getRepository('MainBundle:Quartiers')->find(intval($request->get('quartier')));
          $newHote->setQuartier($quartier);
        }
        $em->persist($newHote);
        $em->flush();
        return $this->redirect($this->generateUrl('admin_hote_list'));
      }
    }
    return $this->render('AdminBundle:Hote:add.html.twig', array(
      'form' => $form->createView(),
    ));
  }

  public function listAction()
  {
    $em = $this->getDoctrine()->getManager();
    $hotes = $em->getRepository('AdminBundle:Hote')->findAll();
    return $this->render('AdminBundle:Hote:list.html.twig', array(
      'hotes'=>$hotes
    ));
  }

  public function editAction($id, Request $request){
    $em = $this->getDoctrine()->getManager();
    $hote = $em->getRepository('AdminBundle:Hote')->find($id);
    $form = $this->createForm(new HoteEditType(), $hote);
    $form->handleRequest($request);
    if($request->getMethod() == 'POST'){
      if($form->isValid()){
        if($request->get('quartier')){
          $quartier = $em->getRepository('MainBundle:Quartiers')->find(intval($request->get('quartier')));
          $hote->setQuartier($quartier);
        }
        $em->persist($hote);
        $em->flush();
        return $this->redirect($this->generateUrl('admin_hote_list'));
      }
    }
    return $this->render('AdminBundle:Hote:edit.html.twig', array(
      'form' => $form->createView(),
      'hote'=>$hote
    ));
  }

  public function deleteAction($id, Request $request){
    $em = $this->getDoctrine()->getManager();
    $hote = $em->getRepository('AdminBundle:Hote')->find($id);
    $em->remove($hote);
    $em->flush();
    return $this->redirect($this->generateUrl('admin_hote_list'));
  }
}
