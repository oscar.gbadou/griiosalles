<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AdminBundle\Entity\TypeEvenement;
use AdminBundle\Form\TypeEvenementType;
use Symfony\Component\HttpFoundation\Request;

class TypeEvenementController extends Controller
{
  public function addAction(Request $request)
  {
    $em = $this->getDoctrine()->getManager();
    $newTypeEvenement = new TypeEvenement();
    $form = $this->createForm(new TypeEvenementType(), $newTypeEvenement);
    $form->handleRequest($request);
    if($request->getMethod() == 'POST'){
      if($form->isValid()){
        $em->persist($newTypeEvenement);
        $em->flush();
        return $this->redirect($this->generateUrl('admin_type_salle_list'));
      }
    }
    return $this->render('AdminBundle:TypeEvenement:add.html.twig', array(
      'form' => $form->createView(),
    ));
  }

  public function editAction($id, Request $request){
    $em = $this->getDoctrine()->getManager();
    $typeEvenement = $em->getRepository('AdminBundle:TypeEvenement')->find($id);
    $form = $this->createForm(new TypeEvenementType(), $typeEvenement);
    $form->handleRequest($request);
    if($request->getMethod() == 'POST'){
      if($form->isValid()){
        $em->persist($typeEvenement);
        $em->flush();
        return $this->redirect($this->generateUrl('admin_type_salle_list'));
      }
    }
    return $this->render('AdminBundle:TypeEvenement:edit.html.twig', array(
      'form' => $form->createView(),
    ));
  }

  public function deleteAction($id){
    $em = $this->getDoctrine()->getManager();
    $typeEvenement = $em->getRepository('AdminBundle:TypeEvenement')->find($id);
    $em->remove($typeEvenement);
    $em->flush();
    return $this->redirect($this->generateUrl('admin_type_salle_list'));
  }
}
