<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AdminBundle\Entity\TypeSalle;
use AdminBundle\Form\TypeSalleType;
use Symfony\Component\HttpFoundation\Request;

class TypeSalleController extends Controller
{
  public function addAction(Request $request)
  {
    $em = $this->getDoctrine()->getManager();
    $newTypeSalle = new TypeSalle();
    $form = $this->createForm(new TypeSalleType(), $newTypeSalle);
    $form->handleRequest($request);
    if($request->getMethod() == 'POST'){
      if($form->isValid()){
        $em->persist($newTypeSalle);
        $em->flush();
        return $this->redirect($this->generateUrl('admin_type_salle_list'));
      }
    }
    return $this->render('AdminBundle:TypeSalle:add.html.twig', array(
      'form' => $form->createView(),
    ));
  }

  public function listAction()
  {
    $em = $this->getDoctrine()->getManager();
    $typeSalles = $em->getRepository('AdminBundle:TypeSalle')->findAll();
    $typeEvenements = $em->getRepository('AdminBundle:TypeEvenement')->findAll();
    $equipements = $em->getRepository('AdminBundle:Equipement')->findAll();
    return $this->render('AdminBundle:TypeSalle:list.html.twig', array(
      'typeSalles'=>$typeSalles,
      'typeEvenements'=>$typeEvenements,
      'equipements'=>$equipements,
    ));
  }

  public function editAction($id, Request $request){
    $em = $this->getDoctrine()->getManager();
    $typeSalle = $em->getRepository('AdminBundle:TypeSalle')->find($id);
    $form = $this->createForm(new TypeSalleType(), $typeSalle);
    $form->handleRequest($request);
    if($request->getMethod() == 'POST'){
      if($form->isValid()){
        $em->persist($typeSalle);
        $em->flush();
        return $this->redirect($this->generateUrl('admin_type_salle_list'));
      }
    }
    return $this->render('AdminBundle:TypeSalle:edit.html.twig', array(
      'form' => $form->createView(),
    ));
  }

  public function deleteAction($id){
    $em = $this->getDoctrine()->getManager();
    $typeSalle = $em->getRepository('AdminBundle:TypeSalle')->find($id);
    $em->remove($typeSalle);
    $em->flush();
    return $this->redirect($this->generateUrl('admin_type_salle_list'));
  }
}
