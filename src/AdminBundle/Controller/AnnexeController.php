<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AdminBundle\Entity\Hote;
use AdminBundle\Entity\Annexe;
use AdminBundle\Form\HoteType;
use AdminBundle\Form\AnnexeType;
use Symfony\Component\HttpFoundation\Request;

class AnnexeController extends Controller
{
  public function addAction($id, Request $request)
  {
    $em = $this->getDoctrine()->getManager();
    $hote = $em->getRepository('AdminBundle:Hote')->find($id);
    $newAnnexe = new Annexe();
    $form = $this->createForm(new AnnexeType(), $newAnnexe);
    $form->handleRequest($request);
    if($request->getMethod() == 'POST'){
      if($form->isValid()){
        if($request->get('quartier')){
          $quartier = $em->getRepository('MainBundle:Quartiers')->find(intval($request->get('quartier')));
          $newAnnexe->setQuartier($quartier);
        }
        $newAnnexe->setHote($hote);
        $em->persist($newAnnexe);
        $em->flush();
        return $this->redirect($this->generateUrl('admin_annexe_add', array('id'=>$id)));
      }
    }
    return $this->render('AdminBundle:Annexe:add.html.twig', array(
      'form' => $form->createView(),
      'hote'=>$hote
    ));
  }

  public function listAction()
  {
    $em = $this->getDoctrine()->getManager();
    $annexes = $em->getRepository('AdminBundle:Annexe')->findAll();
    return $this->render('AdminBundle:Annexe:list.html.twig', array(
      'annexes'=>$annexes
    ));
  }

  public function editAction($id, Request $request){
    $em = $this->getDoctrine()->getManager();
    $annexe = $em->getRepository('AdminBundle:Annexe')->find($id);
    $form = $this->createForm(new AnnexeType(), $annexe);
    $form->handleRequest($request);
    if($request->getMethod() == 'POST'){
      if($form->isValid()){
        if($request->get('quartier')){
          $quartier = $em->getRepository('MainBundle:Quartiers')->find(intval($request->get('quartier')));
          $annexe->setQuartier($quartier);
        }
        $em->persist($annexe);
        $em->flush();
        return $this->redirect($this->generateUrl('admin_annexe_list'));
      }
    }
    return $this->render('AdminBundle:Annexe:edit.html.twig', array(
      'form' => $form->createView(),
      'annexe'=>$annexe
    ));
  }

  public function deleteAction($id, Request $request){
    $em = $this->getDoctrine()->getManager();
    $annexe = $em->getRepository('AdminBundle:Annexe')->find($id);
    $em->remove($annexe);
    $em->flush();
    return $this->redirect($this->generateUrl('admin_annexe_list'));
  }

  public function annexeByHoteAction($id){
    $em = $this->getDoctrine()->getManager();
    $hote = $em->getRepository('AdminBundle:Hote')->find($id);
    $annexes = $em->getRepository('AdminBundle:Annexe')->findByHote($hote);
    return $this->render('AdminBundle:Annexe:list.html.twig', array(
      'annexes'=>$annexes
    ));
  }
}
