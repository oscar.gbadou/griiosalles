<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AdminBundle\Utils\Utils;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('AdminBundle:Default:index.html.twig');
    }

    public function quartierByCommuneAction(Request $request){
      $em = $this->getDoctrine()->getManager();
      $id_commune = $request->get('id_commune');
      $commune = $em->getRepository('MainBundle:Communes')->find(intval($id_commune));
      $quartiers = $em->getRepository('MainBundle:Quartiers')->findBy(array('communes'=>$commune), array('label'=>'ASC'));
      $result = array();
      foreach ($quartiers as $q) {
          $result[] = array(
              'id' => $q->getId(),
              'libelle' => $q->getLabel()
          );
      }
      $response = new Response();
      $response->headers->add(array('Access-Control-Allow-Origin' => '*'));
      $response->setContent(Utils::jsonRemoveUnicodeSequences(json_encode($result)));
      return $response;
    }
}
