<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;

class AnnexeType extends AbstractType
{
  /**
  * @param FormBuilderInterface $builder
  * @param array $options
  */
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
    ->add('nom', 'text', array(
      'label'=>'Nom',
      'attr'=>array(
        'class'=>'form-control',
        'style'=>'margin-bottom: 10px'
      )
    ))
    ->add('telephone', 'text', array(
      'label'=>'Téléphone',
      'attr'=>array(
        'class'=>'form-control',
        'style'=>'margin-bottom: 10px'
      )
    ))
    ->add('adresse', 'text', array(
      'label'=>'Adresse',
      'attr'=>array(
        'class'=>'form-control',
        'style'=>'margin-bottom: 10px'
      )
    ))
    ->add('ville', 'entity', array(
      'label' => 'Ville',
      'placeholder' => 'Choisir la ville',
      'attr' => array(
        'class' => 'form-control',
        'style'=>'margin-bottom: 10px'
      ),
      'class' => 'MainBundle:Communes',
      'query_builder' => function (EntityRepository $er) {
        return $er->createQueryBuilder('u')
        ->orderBy('u.label', 'ASC');
      },
      'choice_label' => 'label',
      ))
      ->add('lat', 'text', array(
        'label'=>'Latitude',
        'attr'=>array(
          'class'=>'form-control',
          'style'=>'margin-bottom: 10px'
        )
      ))
      ->add('lon', 'text', array(
        'label'=>'Longitude',
        'attr'=>array(
          'class'=>'form-control',
          'style'=>'margin-bottom: 10px'
        )
      ))
      //->add('quartier')
      ;
    }

    /**
    * @param OptionsResolver $resolver
    */
    public function configureOptions(OptionsResolver $resolver)
    {
      $resolver->setDefaults(array(
        'data_class' => 'AdminBundle\Entity\Annexe'
      ));
    }
  }
