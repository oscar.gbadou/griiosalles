<?php

namespace UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
* User
*
* @ORM\Table(name="user")
* @ORM\Entity(repositoryClass="UserBundle\Repository\UserRepository")
* @ORM\InheritanceType("JOINED")
* @ORM\DiscriminatorColumn(name="type", type="string")
* @ORM\DiscriminatorMap({"admin" = "Admin"})
*/
abstract class User extends BaseUser
{
  /**
  * @var int
  *
  * @ORM\Column(name="id", type="integer")
  * @ORM\Id
  * @ORM\GeneratedValue(strategy="AUTO")
  */
  protected $id;
  
  public function __construct() {
    parent::__construct();
  }

  public function setEmail($email) {
    parent::setEmail($email);
    $this->setUsername($email);
    return $this;
  }
}
